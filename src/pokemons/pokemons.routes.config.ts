import express from 'express';
import {CommonRoutesConfig} from "../common/common.routes.config";
import request from "request";

const POKE_API_URL = "https://pokeapi.co/api/v2/"

export class PokemonsRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'PokemonsRoutes');
    }

    configureRoutes() {

        this.app.route(`/pokemons`)
            .get((req: express.Request, res: express.Response) => {
                request(`${POKE_API_URL}pokemon`, function(error: any, response: any, body: any) {
                    res.send(JSON.parse(body))
                });
            })


        this.app.route(`/pokemons/:id`)
            .all((req: express.Request, res: express.Response, next: express.NextFunction) => {
                next();
            })
            .get((req: express.Request, res: express.Response) => {
                request(`${POKE_API_URL}pokemon/${req.params.id}`, function(error: any, response: any, body: any) {
                    res.send(JSON.parse(body))
                });
            })

        return this.app;
    }
}